FROM golang:1.18-alpine
WORKDIR /home
COPY go.mod go.sum ./
RUN go mod download && go mod verify
COPY . .
ENV PORT=8080
EXPOSE ${PORT}
RUN go build main.go
CMD ["/home/main"]