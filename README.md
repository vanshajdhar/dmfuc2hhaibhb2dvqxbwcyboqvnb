**The application is configurable via environment variables. We should be able to provide following variables on application startup:**

1. api_key used for NASA API requests (variable name API_KEY, default: DEMO_KEY)
2. Limit of concurrent requests to NASA API (variable name CONCURRENT_REQUESTS, default: 5)
3. A port the server is running on (variable name PORT, default: 8080)


**Run directly via docker image**

1. docker build -t myimage .
2. docker container run --name containerName -e API_KEY=KEY_VALUE -e PORT=PORT_VALUE -e CONCURRENT_REQUESTS=REQ_VALUE -p                     HOST_PORT:PORT_VALUE myimage
