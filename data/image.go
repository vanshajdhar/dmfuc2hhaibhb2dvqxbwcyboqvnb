package data

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/helpers"
)

const shortForm = "2006-01-02"

var ApiKey = helpers.GetEnv("API_KEY", "DEMO_KEY")
var Config helpers.Config
var client *http.Client = &http.Client{}

type Image struct {
	Copyright      string `json:"copyright"`
	Date           string `json:"date"`
	Explanation    string `json:"explanation"`
	HdUrl          string `json:"hdurl"`
	MediaType      string `json:"media_type"`
	ServiceVersion string `json:"service_version"`
	Title          string `json:"title "`
	Url            Url    `json:"url"`
}

type Url string

type Urls struct {
	ListUrl []Url `json:"urls"`
}

func (u *Urls) ToJSON(w io.Writer) error {
	encoder := json.NewEncoder(w)
	return encoder.Encode(u)

}

func (u *Urls) FromJson(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(u)
}

func getImages(gather, dateChan chan string, wg *sync.WaitGroup) error {
	defer wg.Done()
	for eachDate := range dateChan {
		//Use the eachDate in API call to NASA
		var imageData Image
		url := fmt.Sprintf(Config.Url, ApiKey, eachDate)
		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			fmt.Printf("client: could not create request: %s\n", err)
			return err
		}
		res, err := client.Do(req)
		if err != nil {
			fmt.Printf("client: error making http request: %s\n", err)
			return err
		}
		if res.StatusCode != 200 {
			if res.StatusCode == 429 {
				return fmt.Errorf("TooManyRequests")
			} else if res.StatusCode == 403 {
				return fmt.Errorf("ApiKeyInvalid")
			} else {
				return fmt.Errorf("NoResponse")
			}
		}
		decoder := json.NewDecoder(res.Body)
		err = decoder.Decode(&imageData)
		if err != nil {
			fmt.Println(err)
		} else {
			gather <- string(imageData.Url)
		}
		res.Body.Close()
	}
	return nil
}

//rather than using *Urls I can use Urls and make the Urls of a specific length
func GetUrls(parsedFromDate, parsedToDate time.Time, urls *Urls) error {
	wg := &sync.WaitGroup{}
	concurrentUsers, err := strconv.Atoi(helpers.GetEnv("CONCURRENT_REQUESTS", "5"))
	if err != nil {
		return err
	}
	gather := make(chan string)
	dateChan := make(chan string, 5)

	if parsedToDate.After(time.Now()) {
		parsedToDate = time.Now()
	}
	var getImageErr error
	wg.Add(concurrentUsers)
	go func() {
		for i := 0; i < concurrentUsers; i++ {
			go func() {
				getImageErr = getImages(gather, dateChan, wg)
			}()
		}
		wg.Wait()
		close(gather)
	}()

	go func() {
		for d := parsedFromDate; !d.After(parsedToDate); d = d.AddDate(0, 0, 1) {
			if getImageErr != nil {
				if getImageErr.Error() == "TooManyRequests" || getImageErr.Error() == "ApiKeyInvalid" {
					break
				}
			}
			dateChan <- d.Format(shortForm)
		}
		close(dateChan)
	}()

	for urlValues := range gather {
		(*urls).ListUrl = append((*urls).ListUrl, Url(urlValues))
	}

	return getImageErr
}
