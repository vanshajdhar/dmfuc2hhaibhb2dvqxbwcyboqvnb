package handlers

import (
	"bytes"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/data"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/helpers"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/messages"
	"golang.org/x/time/rate"
)

const shortForm = "2006-01-02"

var visitors = make(map[string]*visitor)
var mu sync.Mutex

type UrlHandler struct {
	l *log.Logger
}

type visitor struct {
	limiter  *rate.Limiter
	lastSeen time.Time
}

func init() {
	go cleanupVisitors()
}

func NewHandler(logger *log.Logger) *UrlHandler {
	return &UrlHandler{
		l: logger,
	}
}

func getVisitor(ip string) *rate.Limiter {
	mu.Lock()
	defer mu.Unlock()
	v, exists := visitors[ip]
	if !exists {
		r := rate.Every(1 * time.Hour)
		bursts := 25
		if helpers.GetEnv("API_KEY", "DEMO_KEY") != "DEMO_KEY" {
			bursts = 900
		}
		limiter := rate.NewLimiter(r, bursts)
		visitors[ip] = &visitor{limiter, time.Now()}
		return limiter
	}
	v.lastSeen = time.Now()
	return v.limiter
}

func cleanupVisitors() {
	for {
		time.Sleep(time.Minute)
		mu.Lock()
		for ip, v := range visitors {
			if time.Since(v.lastSeen) > time.Hour {
				delete(visitors, ip)
			}
		}
		mu.Unlock()
	}
}

func (u *UrlHandler) GetUrls(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Content-Type", "application/json")
	queryParams := req.URL.Query()
	urls := &data.Urls{}

	errorMessage := &bytes.Buffer{}
	//Validate the from and to date
	fromDate := queryParams.Get("fromDate")
	if fromDate == "" {
		u.l.Println("from date send from request is empty")
		errMsg := &messages.ErrorMessage{"fromDate must be present in the query params"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	}
	parsedFromDate, err := time.Parse(shortForm, fromDate)
	if err != nil {
		u.l.Println("from date format is not correct")
		errMsg := &messages.ErrorMessage{"Incorrect format of fromDate"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	}

	toDate := queryParams.Get("toDate")
	if toDate == "" {
		u.l.Println("to date send from request is empty")
		errMsg := &messages.ErrorMessage{"toDate must be present in the query params"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	}
	parsedToDate, err := time.Parse(shortForm, toDate)
	if err != nil {
		u.l.Println("to date format is not correct")
		errMsg := &messages.ErrorMessage{"Incorrect format of toDate"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	}

	if parsedFromDate.After(parsedToDate) {
		u.l.Println("from date is after to date")
		errMsg := &messages.ErrorMessage{"fromDate must be less than toDate"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	} else if parsedFromDate.After(time.Now()) {
		u.l.Println("from date cannot be in the future")
		errMsg := &messages.ErrorMessage{"fromDate must be before present date"}
		errMsg.ToJson(errorMessage)
		http.Error(resp, errorMessage.String(), http.StatusBadRequest)
		return
	}

	err = data.GetUrls(parsedFromDate, parsedToDate, urls)
	if err != nil {
		if err.Error() == "TooManyRequests" {
			u.l.Println("Limit exceeded to NASA API", err)
			http.Error(resp, "Limit Exceeded to NASA API", http.StatusTooManyRequests)
		} else if err.Error() == "ApiKeyInvalid" {
			u.l.Println("Invalid NASA API Key", err)
			http.Error(resp, "Invalid NASA API Key", http.StatusForbidden)
		} else {
			u.l.Println("Error from getting urls via NASA API: ", err)
			http.Error(resp, "Incorrect request", http.StatusBadRequest)
		}
		return
	}
	err = urls.ToJSON(resp)
	if err != nil {
		http.Error(resp, "Unable to marshal", http.StatusInternalServerError)
		return
	}
}

func (u *UrlHandler) MiddlewareRateLimiter(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Print(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		limiter := getVisitor(ip)
		u.l.Print("Last Seen: ", visitors[ip].lastSeen)
		if !limiter.Allow() {
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	})
}
