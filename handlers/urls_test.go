package handlers

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/data"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/helpers"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/messages"
)

var logger *log.Logger = log.New(os.Stdout, "Loggin: ", log.LstdFlags)
var urlHandler *UrlHandler = NewHandler(logger)

func TestGetUrlsNegative(t *testing.T) {

	tests := []struct {
		name               string
		fromDate           string
		toDate             string
		expectedStatusCode int
		expectedResponse   string
	}{
		{"IncorrectFromDate", "20221002", "2022-10-16", 400, `{"error":"Incorrect format of fromDate"}`},
		{"IncorrectToDate", "2020-10-12", "2022-10-", 400, `{"error":"Incorrect format of toDate"}`},
		{"EmptyFromDate", "", "2022-10-12", 400, `{"error":"fromDate must be present in the query params"}`},
		{"EmptyToDate", "2020-10-12", "", 400, `{"error":"toDate must be present in the query params"}`},
		{"FromDateGreaterThanToDate", "2020-10-12", "2020-06-12", 400, `{"error":"fromDate must be less than toDate"}`},
		{"FromDateMissingInQueryParams", "exclude", "2022-06-12", 400, `{"error":"fromDate must be present in the query params"}`},
		{"ToDateMissingInQueryParams", "2020-10-12", "exclude", 400, `{"error":"toDate must be present in the query params"}`},
		{"FromDateIsInFuture", "2024-10-12", "2024-12-12", 400, `{"error":"fromDate must be before present date"}`},
	}

	for _, tc := range tests {
		var req *http.Request
		if tc.fromDate == "exclude" {
			req = httptest.NewRequest(http.MethodGet, "/pictures?toDate="+tc.toDate, nil)
		} else if tc.toDate == "exclude" {
			req = httptest.NewRequest(http.MethodGet, "/pictures?fromDate="+tc.fromDate, nil)
		} else {
			path := fmt.Sprintf("/pictures?fromDate=%s&toDate=%s", tc.fromDate, tc.toDate)
			req = httptest.NewRequest(http.MethodGet, path, nil)
		}

		w := httptest.NewRecorder()
		urlHandler.GetUrls(w, req)

		res := w.Result()
		defer res.Body.Close()

		statusCodeGot := res.StatusCode
		dataGot, err := ioutil.ReadAll(res.Body)

		if err != nil {
			t.Errorf("Expected error to be nil but got %v\n", err)
		}

		if statusCodeGot != tc.expectedStatusCode {
			t.Errorf("Scenario Name : %v, Failure Reason: StatusCode Mismatch, Expected: %d, Got: %d", tc.name, tc.expectedStatusCode, statusCodeGot)
		}

		expectedStruct := &messages.ErrorMessage{}
		actualStruct := &messages.ErrorMessage{}

		err = expectedStruct.FromJson(strings.NewReader(string(tc.expectedResponse)))
		if err != nil {
			t.Errorf("Expected error to be nil but got %v\n", err)
		}
		err = actualStruct.FromJson(strings.NewReader(string(dataGot)))
		if err != nil {
			t.Errorf("Expected error to be nil but got %v\n", err)
		}
		if expectedStruct.Message != actualStruct.Message {
			t.Errorf("Scenario Name : %v, Failure Reason: Response Mismatch, Expected Response: %s, Got Response: %s", tc.name, tc.expectedResponse, string(dataGot))
		}
	}
}

func TestGetUrlsPositive(t *testing.T) {
	testCases := []struct {
		name                   string
		fromDate               string
		toDate                 string
		expectedStatusCode     int
		expectedResponseLength int
	}{
		{"FromDate20LessThanToDate", "2020-12-01", "2020-12-02", 200, 2},
		{"FromDateEqualToToDate", "2020-12-01", "2020-12-01", 200, 1},
	}
	for _, tc := range testCases {
		data.Config.Url = "https://api.nasa.gov/planetary/apod?api_key=%s&date=%s"
		data.ApiKey = helpers.GetEnv("API_KEY", "DEMO_KEY")
		var req *http.Request
		path := fmt.Sprintf("/pictures?fromDate=%s&toDate=%s", tc.fromDate, tc.toDate)
		req = httptest.NewRequest(http.MethodGet, path, nil)
		w := httptest.NewRecorder()
		urlHandler.GetUrls(w, req)

		res := w.Result()
		defer res.Body.Close()

		statusCodeGot := res.StatusCode
		dataGot, err := ioutil.ReadAll(res.Body)

		if err != nil {
			t.Errorf("Expected error to be nil but got %v\n", err)
		}

		if statusCodeGot != tc.expectedStatusCode {
			t.Errorf("Scenario Name : %v, Failure Reason: StatusCode Mismatch, Expected: %d, Got: %d", tc.name, tc.expectedStatusCode, statusCodeGot)
		}
		actualStruct := &data.Urls{}
		err = actualStruct.FromJson(strings.NewReader(string(dataGot)))
		if err != nil {
			t.Errorf("Expected error to be nil but got %v\n", err)
		}

		actualNumberOfUrls := len(actualStruct.ListUrl)
		if actualNumberOfUrls != tc.expectedResponseLength {
			t.Errorf("Scenario Name : %v, Failure Reason: Number of urls returned mismatch, Expected number of urls: %d, Got urls: %d", tc.name, tc.expectedResponseLength, actualNumberOfUrls)
		}

	}

}
