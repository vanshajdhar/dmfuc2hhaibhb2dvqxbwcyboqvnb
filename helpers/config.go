package helpers

import "github.com/spf13/viper"

type Config struct {
	Url string `mapstructure:URL`
}

func LoadConfig(path string, config *Config) error {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(config)
	return err
}
