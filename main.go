package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/data"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/handlers"
	"gitlab.com/vanshajdhar/dmfuc2hhaibhb2dvqxbwcyboqvnb/helpers"
)

func main() {
	err := helpers.LoadConfig(".", &data.Config)
	port := fmt.Sprintf(":%s", helpers.GetEnv("PORT", "8080"))
	logger := log.New(os.Stdout, "Loggin: ", log.LstdFlags)
	urlHandler := handlers.NewHandler(logger)

	//Register handler with the server
	imageMuxHandler := mux.NewRouter()
	getRouter := imageMuxHandler.Methods("GET").Subrouter()
	getRouter.HandleFunc("/pictures", urlHandler.GetUrls)

	svr := &http.Server{
		Addr:         port,
		Handler:      urlHandler.MiddlewareRateLimiter(imageMuxHandler),
		IdleTimeout:  120,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	logger.Println("Starting server on port ", port)
	err = svr.ListenAndServe()
	if err != nil {
		logger.Printf("Error starting server %s\n", err)
		os.Exit(1)
	}

}
