package messages

import (
	"encoding/json"
	"io"
)

type ErrorMessage struct {
	Message string `json:"error"`
}

func (e *ErrorMessage) ToJson(w io.Writer) error {
	encoder := json.NewEncoder(w)
	return encoder.Encode(e)
}

func (e *ErrorMessage) FromJson(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(e)
}
